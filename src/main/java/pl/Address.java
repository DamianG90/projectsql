package pl;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

    private String city;
    private String zipCode;
    private String street;
    private int homeNumber;
}
