package pl;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class Person {

    private String name;
    private String surname;
    private int age;
    private int height;
    private Double weight;

    @Embedded
    private Address address;


}
